import requests
from bs4 import BeautifulSoup
def parser():
    response = requests.get('https://chebistok.ru/')
    response_data = BeautifulSoup(response.text, 'lxml')
    data = response_data.find('div', {'class': 'wrap_price'}).find('p', {'class': 'price'})
    return(data.text.split()[0])
